# Wardrobify

Team:

* Elena - Hats
* Yoo Kim - Shoes

## Hats microservices

## Shoes microservice

Created a BinVO and a shoe model to combine and create a microservice that handles the management of shoes in the bins. Microservice can add shoes, check bin quantities, retrieve information on the shoes, and update the location of the shoes.

## Hats microservice

Created a LocationVO and a hat model to combine and create a microservice that handles the management of hats in the locations. Microservice can add hats, retrieve information on the hats, and update the location of the hats.
