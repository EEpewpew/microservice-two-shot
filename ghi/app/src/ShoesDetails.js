import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

function ShoeDetails() {
  const [shoe, setShoe] = useState([]);
  const {id} = useParams();

  async function fetchShoe() {
    try {
      const response = await fetch(`http://localhost:8080/api/shoes/${id}`);

      if (response.ok) {
        const data = await response.json();
        console.log(data)
        setShoe(data);
      } else {
        console.error('Failed to retrieve shoe details');
      }
    } catch (error) {
      console.error('An error occurred while retrieving shoe details:', error);
    }
  }

  useEffect(() => {
    fetchShoe();
  }, []);

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Brand</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
          </tr>
        </thead>
        <tbody>
          <tr key={shoe?.id}>
            <td>{shoe?.manufacturer}</td>
            <td>{shoe?.model_name}</td>
            <td>{shoe?.color}</td>
            <td>
              <img src={shoe?.picture} width = "300"/>
            </td>
            <td>{shoe?.bin}</td>
          </tr>
        </tbody>
      </table>
    );
  }

export default ShoeDetails;
