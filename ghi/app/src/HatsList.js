import React, {useEffect, useState} from 'react';
import HatDetails from './HatDetails';
import deleteHat from './DeleteHat';
import {Link} from "react-router-dom";

function HatsList() {
    const [hats, setHats] = useState([]);

    async function fetchHats() {
        const response = await fetch("http://localhost:8090/api/hats")

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    }

    useEffect (() => {
        fetchHats();
    }, []);
    
    async function handleDeleteHat(hatId) {
        await deleteHat(hatId);
        fetchHats();
      }
    
      return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Fabric</th>
              <th>Style_name</th>
              <th>Actions</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {hats.map((hat) => {
              return (
                <tr key={hat.id}>
                  <td>{hat.fabric}</td>
                  <td>{hat.style_name}</td>
                  <td>
                    <Link className="nav-link active" aria-current="page" to={`${hat.id}`}><button>View Details</button></Link>
                  </td>
                  <td><button onClick={() => handleDeleteHat(hat.id)}>Delete</button></td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    }
    
export default HatsList;