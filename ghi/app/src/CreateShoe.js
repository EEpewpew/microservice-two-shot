window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8080/api/shoes/';

    const response = await fetch(url);

    if(response.ok){

        const data = await response.json();

    }

    const formTag = document.getElementById('create-shoe-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const binUrl = 'http://localhost:8100/api/bins/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(binUrl, fetchConfig);
        if (response.ok) {
        formTag.reset();
        const newShoe = await response.json();
        }
    });
});
