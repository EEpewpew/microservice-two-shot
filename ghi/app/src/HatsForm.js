import React, { useEffect, useState } from 'react';

function HatsForm() {
  const [locations, setLocations] = useState([])

  const [formData, setFormData] = useState({
    color: '',
    fabric: '',
    style_name: '',
    picture_url: '',
    location: '',
  })

  const fetchData = async () => {
    const locationUrl = 'http://localhost:8100/api/locations/';
    const response = await fetch(locationUrl);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
      console.log(data.locations)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const hatUrl = 'http://localhost:8090/api/hats/';
    console.log(formData)

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(hatUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        color: '',
        fabric: '',
        style_name: '',
        picture_url: '',
        location: '',
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new Hat</h1>
        <form onSubmit={handleSubmit} id="create-hat-form">

          <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Color" required type="text" name="color" className="form-control" />
            <label htmlFor="name">Color</label>
          </div>

          <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Fabric" required type="text" name="fabric" className="form-control" />
            <label htmlFor="starts">Fabric</label>
          </div>

          <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Style Name" required type="text" name="style_name" className="form-control" />
            <label htmlFor="ends">Style Name</label>
          </div>

          <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Picture Url" required type="url" name="picture_url" className="form-control" />
            <label htmlFor="picture_url">Picture Url</label>
            <select onChange={handleFormChange} required name="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                return (
                <option key={location.id} value={location.id}>{location.closet_name}</option>
                )
              })}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
)
}

export default HatsForm;
