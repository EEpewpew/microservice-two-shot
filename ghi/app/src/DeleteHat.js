async function deleteHat(hatId) {
    try {
      const response = await fetch(`http://localhost:8090/api/hats/${hatId}`, {
        method: 'DELETE',
      });
  
      if (response.ok) {
        console.log('Hat deleted successfully');
        
      } else {
        console.error('Failed to delete the hat');
        
      }
    } catch (error) {
      console.error('An error occurred while deleting the hat:', error);
      
    }
  }
  
  export default deleteHat;