async function deleteShoe(shoeId) {
    try {
      const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}`, {
        method: 'DELETE',
      });

      if (response.ok) {
        console.log('Shoe deleted successfully');

      } else {
        console.error('Failed to delete the shoe');

      }
    } catch (error) {
      console.error('An error occurred while deleting the shoe:', error);

    }
  }

  export default deleteShoe;
