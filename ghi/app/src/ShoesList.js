import React, {useEffect, useState} from 'react';
import ShoesDetails from './ShoesDetails';
import deleteShoe from './DeleteShoe';
import {Link} from "react-router-dom";

function ShoesList() {
    const [shoes, setShoes] = useState([]);

    async function fetchShoes() {
        const response = await fetch("http://localhost:8080/api/shoes")

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    }

    useEffect (() => {
        fetchShoes();
    }, []);


    async function handleGetShoesDetails(shoeId) {
        const shoesDetails = await ShoesDetails(shoeId);
        if (shoesDetails) {
          console.log('Shoe details:', shoesDetails);

        } else {
          console.error('Failed to retrieve shoe details');

        }
      }

    async function handleDeleteShoe(shoeId) {
        await deleteShoe(shoeId);
        fetchShoes();
      }

      return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Brand</th>
              <th>Details</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {shoes.map((shoe) => {
              return (
                <tr key={shoe.id}>
                  <td>{shoe.model_name}</td>
                  <td>{shoe.manufacturer}</td>
                  <td>
                    <Link className="nav-link active" aria-current="page" to={`${shoe.id}`}><button>View Details</button></Link>
                  </td>
                  <td>
                    <button onClick={() => handleDeleteShoe(shoe.id)}>Delete</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    }

export default ShoesList;
