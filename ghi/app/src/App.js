import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList'
import ShoesForm from './ShoesForm'
import HatsForm from './HatsForm'
import HatsList from './HatsList'
import HatDetails from './HatDetails';
import ShoesDetails from './ShoesDetails';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="" element={<ShoesList />} />
            <Route path="new" element={<ShoesForm />} />
            <Route path=":id" element={<ShoesDetails />} />
          </Route>
            <Route path="hats">
              <Route path="" element={<HatsList />} />
              <Route path="new" element={<HatsForm />} />
              <Route path=":id" element={<HatDetails />} />
            </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
