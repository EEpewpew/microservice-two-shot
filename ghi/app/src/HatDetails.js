import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

function HatDetails() {
    const [hat, setHat] = useState([]);
    const {id}=useParams();

    async function fetchHat() {
        try {
            const response = await fetch(`http://localhost:8090/api/hats/${id}`);

            if (response.ok) {
                const data = await response.json();
                console.log(data)
                setHat(data);
            } else {
                console.error('Failed to retrieve hat details');
            }
        } catch (error) {
            console.error('An error occurred while retrieving hat details:', error);
        }
    }

    useEffect(() => {
        fetchHat();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style_name</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                <tr key={hat?.id}>
                    <td>{hat?.fabric}</td>
                    <td>{hat?.style_name}</td>
                    <td>{hat?.color}</td>
                    <td>
                        <img src={hat?.picture_url} width = "300"/>
                    </td>
                    <td>{hat?.location?.closet_name}</td>
                </tr>
            </tbody>
        </table>
    );
}

export default HatDetails;